<?php
/*
 Plugin Name: Three Part Encryption (Library)
 Description: This plugin adds 3 PHP classes which can be used to encrypt/decrypt information in a secure 3-part key system.
 Author: Brian Neff
 License: GPLv2 or later
 Version: 1.0.0
 */

require_once 'classes/Encryption.class.php';
require_once 'classes/EncryptedData.class.php';
require_once 'classes/PackedData.class.php';

//this plugin does nothing on its own. to see how to use it, look in the examples folder