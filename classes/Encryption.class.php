<?php
namespace ThreePartEncryption;

class Encryption {
	
	private $_key1;
	private $_key2;
	private $_password;
	
	public function __construct( $key1 , $key2 , $password ) {
		$this->_key1 = $key1;
		$this->_key2 = $key2;
		$this->_password = $password;
	}
	
	/**
	 * @param string $data The unencrypted data to encrypt and pack.
	 * @return \ThreePartEncryption\PackedData An instance of the EncryptedData object holding the encrypted data.
	 */
	public function pack( $data ) {
		
		$_encryptedData = $this->encrypt( $data );
		
		return new PackedData( $_encryptedData->getIv() , $this->_key1 , $this->_key2 , $this->_password, $_encryptedData->getData() );
		
	}
	
	/**
	 * @param string $data The data to encrypt.
	 * @return \ThreePartEncryption\EncryptedData An instance of the EncryptedData object holding the encrypted data.
	 */
	public function encrypt( $data ) {
	
		/* Open the cipher */
		$td = mcrypt_module_open('rijndael-256', '', 'cfb', '');
	
		/* Create the IV and determine the keysize length, use MCRYPT_RAND
		 * on Windows instead */
		$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
		
		$ks = mcrypt_enc_get_key_size($td);
	
		/* Create key */
		$key1 = sha1($this->_key1.$this->_password);
		$key2 = sha1($this->_key2.$this->_password);
	
		$key = substr( $key1 , 0 , floor( $ks/2 ) ) . substr( $key2 , 0 , ceil( $ks/2 ) );
	
		/* Intialize encryption */
		mcrypt_generic_init($td, $key, $iv);
	
		/* Encrypt data */
		$encrypted = mcrypt_generic($td, $data);
		
		/* Terminate encryption handler */
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		
		return new EncryptedData( $iv , $encrypted );
	
	}
	
	
	public function decrypt( $encryptedData ) {
	
		/* Open the cipher */
		$td = mcrypt_module_open('rijndael-256', '', 'cfb', '');
	
		/* Determine the keysize length */
		$ks = mcrypt_enc_get_key_size($td);
	
		/* Create key */
		$key1 = sha1($this->_key1.$this->_password);
		$key2 = sha1($this->_key2.$this->_password);
	
		$key = substr( $key1 , 0 , floor( $ks/2 ) ) . substr( $key2 , 0 , ceil( $ks/2 ) );
	
		/* Initialize encryption module for decryption */
		mcrypt_generic_init( $td , $key , $encryptedData->getIv() );
	
		/* Decrypt encrypted string */
		$decrypted = mdecrypt_generic( $td , $encryptedData->getData() );
	
		/* Terminate decryption handle and close module */
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
	
		return $decrypted;
	
	}
	
}