<?php
namespace ThreePartEncryption;

class EncryptedData {
	
	private $_iv;
	private $_data;
	
	public function __construct( $iv , $data ) {
		
		$this->_iv = $iv;
		$this->_data = $data;
		
	}
	/**
	 * @return the $_iv
	 */
	public function getIv() {

		return $this->_iv;
	}


	/**
	 * @return the $_data
	 */
	public function getData() {

		return $this->_data;
	}


	
	
	
}