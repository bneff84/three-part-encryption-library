<?php
namespace ThreePartEncryption;

class PackedData {
	
	private $_iv;
	private $_key1;
	private $_key2;
	private $_password;
	private $_data;
	
	private $_dataPieceSize = 64;
	
	
	/**
	 * @param string|number $iv The IV used for this encrypted data.
	 * @param string $key1 The first part of the key.
	 * @param string $key2 The second part of the key.
	 * @param string $password The static password for the key.
	 * @param string $data The encrypted data chunk.
	 */
	public function __construct( $iv , $key1 , $key2 , $password , $data ) {
		$this->_iv = $iv;
		$this->_key1 = $key1;
		$this->_key2 = $key2;
		$this->_password = $password;
		$this->_data = $data;
	}
	
	/**
	 * @return string The data, minus the datapiece from the beginning.
	 */
	public function getData() {
		return substr( $this->_data , $this->_dataPieceSize );
	}
	
	/**
	 * @return string Returns the datapiece from the start of the encrypted data.
	 */
	public function getDataPiece() {
		return substr( $this->_data , 0 , $this->_dataPieceSize );
	}
	
	/**
	 * @return number Returns the size of the datapiece for this form.
	 */
	public function getDataPieceSize() {
		return $this->_dataPieceSize;
	}
	
	/**
	 * @param number $size Sets the size of the datapiece for this form.
	 */
	public function setDataPieceSize( $size ) {
		$this->_dataPieceSize = $size;
	}
	
	/**
	 * @return string The IV used in this form.
	 */
	public function getIv() {
		return $this->_iv;
	}
	
	/**
	 * @return string The first key used in this form.
	 */
	public function getKey1() {
		return $this->_key1;
	}
	
	/**
	 * @return string The second key used in this form.
	 */
	public function getKey2() {
		return $this->_key2;
	}
	
	/**
	 * @return string The password used in this form.
	 */
	public function getPassword() {
		return $this->_password;
	}
	
	/**
	 * Decrypt the packed data and return the original.
	 * @return string The decrypted string.
	 */
	public function unpack() {
		
		$_encryption = new Encryption( $this->_key1 , $this->_key2 , $this->_password );
		$_encryptedData = new EncryptedData( $this->_iv , $this->_data );
		
		return $_encryption->decrypt( $_encryptedData );
	
	}
	
	/**
	 * This is here because serializing this object for direct database storage completely negates the encryption used. Don't do it.
	 */
	public function __sleep() {
		trigger_error( "Do not serialize the PackedForm object for database storage. It contains all information needed to decrypt your data, and stores that information in plaintext when serialized." , E_USER_ERROR );
		exit;
	}
	
}