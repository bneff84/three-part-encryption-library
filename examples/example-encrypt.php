<?php
use ThreePartEncryption\Encryption;
require_once '../classes/Encryption.class.php';
require_once '../classes/EncryptedData.class.php';
require_once '../classes/PackedData.class.php';

if( $_POST['data'] ) {
	
	//the data to encrypt
	$data = $_POST['data']['data'];
	
	//the password from the data
	$password = $_POST['data']['password'];
	
	//two randomly generated key parts that should change with each encryption
	//Note: This is not the most secure method of generating random keys.
	$key1 = sha1( mt_rand( 0, pow(2,30) ) . mt_rand( 0, pow(2,30) ) );
	$key2 = sha1( mt_rand( 0, pow(2,30) ) . mt_rand( 0, pow(2,30) ) );
	
	$encryption = new Encryption($key1, $key2, $password);
	
	$packedData = $encryption->pack($data);
	
	?>
	
<pre>
The database would contain
	> data: <?php echo base64_encode( $packedData->getData() ); ?>

	> key2: <?php echo $key2; ?>


The notice to the user would contain
	> dataPiece: <?php echo base64_encode( $packedData->getDataPiece() ); ?>

	> iv: <?php echo base64_encode( $packedData->getIv() ); ?>

	> key1: <?php echo $key1; ?>


The password is not communicated to the user or stored in the database.
Instead it should be a fixed value that either does not change or changes
at a scheduled interval and is communicated to the user securely.

The password you used was: <?php echo $password; ?>


To decrypt your data, type that password into the form below.
</pre>
<form action="example-decrypt.php" method="post">
	<input type="hidden" name="data[data]" value="<?php echo base64_encode( $packedData->getData() ); ?>" />
	<input type="hidden" name="data[key2]" value="<?php echo $key2; ?>" />
	<input type="hidden" name="data[dataPiece]" value="<?php echo base64_encode( $packedData->getDataPiece() ); ?>" />
	<input type="hidden" name="data[iv]" value="<?php echo base64_encode( $packedData->getIv() ); ?>" />
	<input type="hidden" name="data[key1]" value="<?php echo $key1; ?>" />
	<label>
		Enter your password
		<input type="text" name="data[password]" />
	</label>
	<button type="submit">Decrypt</button>
</form>
	
	<?php
	
} else {

?>
<form action="example-encrypt.php" method="post">
	<label>
		Choose a Password
		<input type="text" name="data[password]" />
	</label>
	<label>
		Data (enter some text)
		<textarea name="data[data]">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget nisl convallis, mollis arcu in, placerat leo. Phasellus molestie lacinia turpis. Phasellus molestie nisi ex, sed aliquet nibh luctus eget. Cras lacinia lorem elementum, iaculis magna ut, tincidunt nisl. Mauris justo odio, bibendum et sapien eu, iaculis consequat purus. Pellentesque sed mauris enim. Donec non mauris magna. Maecenas euismod at orci sed dictum. Aenean et sapien lectus. Nam cursus condimentum commodo. Cras non massa eros. Donec sagittis in enim nec viverra. Quisque sed lectus augue. Nunc nec lectus non turpis blandit sagittis eget eget purus.</textarea>
	</label>
	<div>
		<button type="submit">Encrypt It</button>
	</div>
</form>
<?php
	
}