<?php
use ThreePartEncryption\Encryption;
require_once '../classes/Encryption.class.php';
require_once '../classes/EncryptedData.class.php';
require_once '../classes/PackedData.class.php';

//what we want to encrypt
$data = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque quis urna varius, imperdiet felis non, posuere diam. Suspendisse et volutpat metus, in gravida tellus. Nulla facilisi. Vivamus sed fringilla orci, at varius lorem. Duis eu nisi vitae diam luctus dictum sed sit amet mauris. Suspendisse pretium mi ipsum, at maximus libero rutrum id. Nullam nisi ante, dignissim sed iaculis dictum, blandit non metus. Mauris ipsum sem, dapibus at sodales a, vulputate et orci. Sed eu lacus pulvinar, maximus nibh in, molestie orci. Phasellus scelerisque, mauris at consectetur maximus, sem mauris molestie arcu, nec ornare risus nibh sit amet libero.";

//a user-readable password to use as part of the key. typically this would remain static or change on a rotating time period and should be communicated to the user outside of whatever system relays the encrypted data to them
$password = "the quick brown fox jumped over the lazy dog";

//two randomly generated key parts that should change with each encryption
//Note: This is not the most secure method of generating random keys.
$key1 = sha1( mt_rand( 0, pow(2,30) ) . mt_rand( 0, pow(2,30) ) );
$key2 = sha1( mt_rand( 0, pow(2,30) ) . mt_rand( 0, pow(2,30) ) );

//instance the encryption class with the keys and password
$encryption = new Encryption($key1, $key2, $password);

//encrypt and pack the data
$packedData = $encryption->pack($data);

//now you would store the encrypted data (minus the data piece) and the second part of the key in the database, which would return some form of ID for the data
$databaseData = array(
	'data' => $packedData->getData(), //this should probably be a BLOB field
	'key2' => $key2, //this will be a string most likely
);

//mock data ID
$databaseDataId = "412";

//assemble a url which can be used by your user to decrypt and view this information securely, send this to them via email
$url = "http://example.com/decrypt.php?data-id={$databaseDataId}&data-piece=" . base64_encode( $packedData->getDataPiece() ) . "&key1={$key1}&iv=" . base64_encode( $packedData->getIv() );

//for a working example of encryption/decryption, access the example-encrypt.php file