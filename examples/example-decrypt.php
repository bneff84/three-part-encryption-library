<?php
use ThreePartEncryption\Encryption;
use ThreePartEncryption\PackedData;
require_once '../classes/Encryption.class.php';
require_once '../classes/EncryptedData.class.php';
require_once '../classes/PackedData.class.php';

if( $_POST['data'] ) {
	
	$data =& $_POST['data'];
	
	//recreate the packed data object, it is important to concatenate the dataPiece and data strings together after decoding them. The dataPiece always goes on the front.
	$packedData = new PackedData( base64_decode( $data['iv'] ) , $data['key1'], $data['key2'], $data['password'], base64_decode( $data['dataPiece'] ) . base64_decode( $data['data'] ) );
	
	$originalData = $packedData->unpack();
	
	?>
	
<pre>
The original data was:
<?php echo $originalData; ?>
</pre>
	<?php
	
} else {

?>
This page can not be directly accessed, visit <a href="example-encrypt.php">example-encrypt.php</a> instead.
<?php
	
}